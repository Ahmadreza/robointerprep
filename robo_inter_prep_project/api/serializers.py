from dataclasses import fields
from requests import request
from rest_framework import serializers
from rest_framework.reverse import reverse
from api.models import Job, Topic, Interview, Question, Answer, Course


class JobSerializer(serializers.ModelSerializer):
    topics_count = serializers.SerializerMethodField(read_only=True)
    questions_count = serializers.SerializerMethodField(read_only=True)
    update_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Job
        fields = ('id', 'title', 'description', 'priority', 'image',
                  'yt_video', 'icon', 'creator', 'created_at', 'archived',
                  'topics_count', 'questions_count', 'update_url')

    def get_topics_count(self, obj):
        return len(obj.topic_set.all())

    def get_questions_count(self, obj):
        count = 0
        for t in obj.topic_set.all():
            count += len(t.question_set.all())
        return count

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('job-update', kwargs={"pk": obj.pk}, request=request)

class TopicSerializer(serializers.ModelSerializer):
    questions_count = serializers.SerializerMethodField(read_only=True)
    update_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Topic
        fields = ('id', 'title', 'description', 'priority', 'image',
                  'yt_video', 'job', 'creator', 'created_at', 'archived',
                  'questions_count', 'update_url')

    def get_questions_count(self, obj):
        return len(obj.question_set.all())

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('topic-update', kwargs={"pk": obj.pk}, request=request)

class InterviewSerializer(serializers.ModelSerializer):   
    update_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Interview
        fields = ('id', 'title', 'description', 'yt_video', 'contains_coding', 
                  'is_default_interview', 'level', 'job', 'topic', 'creator',
                  'created_at', 'archived', 'update_url')

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('interview-update', kwargs={"pk": obj.pk}, request=request)

class QuestionSerializer(serializers.ModelSerializer):
    answer = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='description'
     )
    update_url = serializers.SerializerMethodField(read_only=True)
    answer_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Question
        fields = ('id', 'title', 'description', 'priority', 'yt_video',
                  'contains_code', 'duration_minutes', 'type', 'level', 
                  'interview', 'topic', 'creator', 'created_at', 'answer', 
                  'archived', 'update_url', 'answer_url')

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('question-update', kwargs={"pk": obj.pk}, request=request)

    def get_answer_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('answer-detail', kwargs={"pk": obj.pk}, request=request)

class AnswerSerializer(serializers.ModelSerializer):
    update_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Answer
        fields = ('id', 'title', 'description', 'yt_video', 'type', 
                  'question', 'creator', 'created_at', 'archived', 'update_url')

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('answer-update', kwargs={"pk": obj.pk}, request=request)

class CourseSerializer(serializers.ModelSerializer):
    update_url = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Course
        fields = ('id', 'title', 'description', 'yt_video', 'interviews', 
                  'topics', 'jobs', 'creator', 'created_at', 'archived', 'update_url')

    def get_update_url(self, obj):
        request = self.context.get('request')
        if (request is None):
            return None
        return reverse('course-update', kwargs={"pk": obj.pk}, request=request)
