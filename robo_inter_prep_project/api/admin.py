from django.contrib import admin
from api.models import Job, Topic, Interview, Question, Answer, Course
# Register your models here.
admin.site.register(Job)
admin.site.register(Topic)
admin.site.register(Interview)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Course)