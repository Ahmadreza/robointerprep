from cgitb import lookup
from datetime import date
from django.db import models
from django.contrib.auth.models import User
from numpy import tile
from django.db.models import Q
DIFFICULTY_LEVELS = (
    ('1', 'Easy'),
    ('2', 'Medium'),
    ('3', 'Hard'),
    ('4', 'Very Hard'),
)

QUESTION_TYPES = (
    ('1', 'simple'),
    ('2', 'complicated')
)

ANSWER_TYPES = (
    ('1', 'text'),
    ('2', 'code'),
    ('3', 'text and code'),
)

JOB_ICONS = (
    ('1', 'DriveEta'),
    ('2', 'PrecisionManufacturing')
)
# Create your models here.

class SearchQuerySet(models.QuerySet):
    
    def is_not_archived(self):
        return self.filter(archived=False)

    def search(self, query, user=None):
        lookup = Q(title__icontains=query) | Q(description__icontains=query)
        qs = self.is_not_archived().filter(lookup)
        return qs

    def filterTopics(self, query, user=None):
        if 'jobID' in query:
            lookup = Q(job__pk=query['jobID'])
            qs = self.is_not_archived().filter(lookup)
            return qs

    def filterQuestions(self, query, user=None):
        if 'topicID' in query:
            lookup = Q(topic__pk=query['topicID'])
            qs = self.is_not_archived().filter(lookup).order_by('priority')
            return qs

    def filterAnswers(self, query, user=None):
        if 'questionID' in query:
            lookup = Q(question__pk=query['questionID'])
            qs = self.is_not_archived().filter(lookup).order_by('question__priority')
            return qs

class SearchManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return SearchQuerySet(self.model, using=self._db)

class Job(models.Model):
    title = models.CharField(max_length=150, default="", unique=True)
    description = models.TextField(default="", blank=True)
    priority = models.PositiveSmallIntegerField(unique=True)
    image = models.ImageField(upload_to='images/jobs', null=True, blank=True)
    yt_video = models.CharField(max_length=300, default="", blank=True)
    icon = models.CharField(
        max_length=15, choices=JOB_ICONS, default="PrecisionManufacturing")
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)
    
    objects = SearchManager()

    def __str__(self):
        return self.title

class Topic(models.Model):
    title = models.CharField(max_length=150, default="", unique=True)
    description = models.TextField(default="", blank=True)
    priority = models.PositiveSmallIntegerField(unique=True)
    image = models.ImageField(upload_to='images/topics', null=True, blank=True)
    yt_video = models.CharField(max_length=300, default="", blank=True)
    job = models.ManyToManyField(Job, blank=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)

    objects = SearchManager()

    def __str__(self):
        return self.title


class Interview(models.Model):
    title = models.CharField(max_length=150, default="")
    description = models.TextField(default="")
    yt_video = models.CharField(max_length=300, default="")
    contains_coding = models.BooleanField(default=False)
    is_default_interview = models.BooleanField(default=False)
    level = models.CharField(
        max_length=15, choices=DIFFICULTY_LEVELS, null=True)
    job = models.ForeignKey(Job, on_delete=models.CASCADE, null=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, null=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)

    objects = SearchManager()
    
    def __str__(self):
        return self.title


class Question(models.Model):
    title = models.CharField(max_length=150, default="")
    description = models.TextField(default="")
    priority = models.PositiveSmallIntegerField(unique=True)
    yt_video = models.CharField(max_length=300, default="")
    contains_code = models.BooleanField(default=False)
    duration_minutes = models.PositiveIntegerField(null=True)
    type = models.CharField(max_length=15, choices=QUESTION_TYPES, default="1")
    level = models.CharField(
        max_length=15, choices=DIFFICULTY_LEVELS, null=True)
    interview = models.ForeignKey(Interview, on_delete=models.CASCADE, null=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, null=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)

    objects = SearchManager()

    def __str__(self):
        return self.title


class Answer(models.Model):
    title = models.CharField(max_length=150, default="")
    description = models.TextField(default="")
    yt_video = models.CharField(max_length=300, default="")
    type = models.CharField(max_length=15, choices=ANSWER_TYPES, default="1")
    question = models.OneToOneField(Question, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)

    objects = SearchManager()

    def __str__(self):
        return self.title

class Course(models.Model):
    title = models.CharField(max_length=200, default="")
    description = models.TextField(default="")
    yt_video = models.CharField(max_length=300, default="")
    interviews = models.ManyToManyField(Interview, blank=True)
    topics = models.ManyToManyField(Topic, blank=True)
    jobs = models.ManyToManyField(Job, blank=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)

    objects = SearchManager()

    def __str__(self):
        return self.title
