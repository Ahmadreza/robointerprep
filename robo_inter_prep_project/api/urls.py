from django.urls import path
from .views import *

urlpatterns = [
    path('job/', JobView.as_view()),
    path('topic/', TopicView.as_view()),
    path('interview/', InterviewView.as_view()),
    path('question/', QuestionView.as_view()),
    path('answer/', AnswerView.as_view()),
    path('course/', CourseView.as_view()),

    path('job/<int:pk>/', JobDetailView.as_view(), name='job-detail'),    
    path('topic/<int:pk>/', TopicDetailView.as_view(), name='topic-detail'),    
    path('interview/<int:pk>/', InterviewDetailView.as_view(), name='interview-detail'),    
    path('question/<int:pk>/', QuestionDetailView.as_view(), name='question-detail'),    
    path('answer/<int:pk>/', AnswerDetailView.as_view(), name='answer-detail'),    
    path('course/<int:pk>/', CourseDetailView.as_view(), name='course-detail'),
    
    path('job/<int:pk>/update/', JobUpdateView.as_view(), name='job-update'),
    path('topic/<int:pk>/update/', TopicUpdateView.as_view(), name='topic-update'),
    path('interview/<int:pk>/update/', InterviewUpdateView.as_view(), name='interview-update'),
    path('question/<int:pk>/update/', QuestionUpdateView.as_view(), name='question-update'),
    path('answer/<int:pk>/update/', AnswerUpdateView.as_view(), name='answer-update'),
    path('course/<int:pk>/update/', CourseUpdateView.as_view(), name='course-update'),


    path('job/<int:pk>/delete/', JobDeleteView.as_view()),
    path('topic/<int:pk>/delete/', TopicDeleteView.as_view()),
    path('interview/<int:pk>/delete/', InterviewDeleteView.as_view()),
    path('question/<int:pk>/delete/', QuestionDeleteView.as_view()),
    path('answer/<int:pk>/delete/', AnswerDeleteView.as_view()),
    path('course/<int:pk>/delete/', CourseDeleteView.as_view()),

    path('job/search/', JobSearchListView.as_view()),
    path('topic/search/', TopicSeachListView.as_view()),
    path('interview/search/', InterviewSeachListView.as_view()),
    path('question/search/', QuestionSeachListView.as_view()),
    path('answer/search/', AnswerSeachListView.as_view()),
    path('course/search/', CourseSeachListView.as_view()),

    path('topic/filter/', TopicsFilterListView.as_view()),
    path('question/filter/', QuestionsFilterListView.as_view()),
    path('answer/filter/', AnswersFilterListView.as_view()),
]
