from unittest import result
from django.shortcuts import render
from requests import request
from rest_framework import generics, permissions
from yaml import serialize
from api.models import Job, Topic, Interview, Question, Answer, Course
from .serializers import JobSerializer, TopicSerializer, InterviewSerializer, QuestionSerializer, AnswerSerializer, CourseSerializer

# Create your views here.
class IsAdminUserOrReadOnly(permissions.IsAdminUser):

    def has_permission(self, request, view):
        is_admin = super().has_permission(request, view)
        # Python3: is_admin = super().has_permission(request, view)
        return request.method in permissions.SAFE_METHODS or is_admin


#//////////////////////// LIST/CREATE APIS ////////////////////////#

class JobView(generics.ListCreateAPIView):
    queryset = Job.objects.all().order_by('priority')
    serializer_class = JobSerializer
    permission_classes = [IsAdminUserOrReadOnly]

class TopicView(generics.ListCreateAPIView):
    queryset = Topic.objects.all().order_by('priority')
    serializer_class = TopicSerializer
    permission_classes = [IsAdminUserOrReadOnly]

class InterviewView(generics.ListCreateAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer
    permission_classes = [IsAdminUserOrReadOnly]

class QuestionView(generics.ListCreateAPIView):
    queryset = Question.objects.all().order_by('priority')
    serializer_class = QuestionSerializer
    permission_classes = [IsAdminUserOrReadOnly]

class AnswerView(generics.ListCreateAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [IsAdminUserOrReadOnly]

class CourseView(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [IsAdminUserOrReadOnly]


#//////////////////////// DETAIL APIS ////////////////////////#

class JobDetailView(generics.RetrieveAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class TopicDetailView(generics.RetrieveAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class QuestionDetailView(generics.RetrieveAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class AnswerDetailView(generics.RetrieveAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class InterviewDetailView(generics.RetrieveAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer

class CourseDetailView(generics.RetrieveAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


#//////////////////////// DELETE APIS ////////////////////////#

class JobDeleteView(generics.DestroyAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class TopicDeleteView(generics.DestroyAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class QuestionDeleteView(generics.DestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class AnswerDeleteView(generics.DestroyAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class InterviewDeleteView(generics.DestroyAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer

class CourseDeleteView(generics.DestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


#//////////////////////// UPDATE APIS ////////////////////////#

class JobUpdateView(generics.UpdateAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class TopicUpdateView(generics.UpdateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class QuestionUpdateView(generics.UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class AnswerUpdateView(generics.UpdateAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class InterviewUpdateView(generics.UpdateAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer

class CourseUpdateView(generics.UpdateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


#//////////////////////// SEARCH APIS ////////////////////////#

class JobSearchListView(generics.ListAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Job.objects.none()
        return results

class TopicSeachListView(generics.ListAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Topic.objects.none()
        return results

class InterviewSeachListView(generics.ListAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Interview.objects.none()
        return results

class QuestionSeachListView(generics.ListAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Question.objects.none()
        return results
        
class AnswerSeachListView(generics.ListAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Answer.objects.none()
        return results

class CourseSeachListView(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        results = Course.objects.none()
        return results        


#//////////////////////// FILTER APIS ////////////////////////#

class TopicsFilterListView(generics.ListAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        jobID = self.request.GET.get('jobID')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        if jobID is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.filterTopics({'jobID': jobID}, user=user)
            return results
        results = Topic.objects.none()

class QuestionsFilterListView(generics.ListAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        topicID = self.request.GET.get('topicID')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        if topicID is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.filterQuestions({'topicID':topicID}, user=user)
            return results
        results = Question.objects.none()

class AnswersFilterListView(generics.ListAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        q = self.request.GET.get('q')
        questionID = self.request.GET.get('questionID')
        if q is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.search(q, user=user)
            return results
        if questionID is not None:
            user = None
            if (self.request.user.is_authenticated):
                user = self.request.user
            results = qs.filterAnswers({'questionID':questionID}, user=user)
            return results
        results = Answer.objects.none()
