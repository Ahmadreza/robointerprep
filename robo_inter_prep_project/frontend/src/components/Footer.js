import * as React from "react";
import {
  Grid,
  Typography,
  Button,
  ButtonGroup,
  IconButton,
  Card,
  Box,
  ListItemButton,
  ListItem,
  ListItemText,
  Link,
  Divider,
} from "@mui/material";
export default function Footer(props) {
  return (
    <Box sx={{ width: "93%", margin: "auto", backgroundColor: "", borderTop: "1px inset rgba(255, 255, 255, 0.4)", paddingTop: "15px", paddingBottom: "15px" }}>
      <Grid
        container
        display="flex"
        alignItems="center"
        justifyContent="center"
        mt={0}
      >
        <Grid item xs={7} sx={{ paddingTop: "0 !important" }}>
          <Typography variant="caption" color={"#757575"}>Copyright &copy; 2022 <Typography component="span"  color={"black"}>RoboInterPrep</Typography></Typography>
        </Grid>
        <Grid item mr={1.3} sx={{ paddingTop: "0 !important" }}>
          <Link href="#" underline="none">
            Help center
          </Link>
        </Grid>
        <Divider light orientation="vertical" flexItem />
        <Grid item mr={1.3} ml={1.3} sx={{ paddingTop: "0 !important" }}>
          <Link href="#" underline="none">
            Jobs
          </Link>
        </Grid>
        <Divider light orientation="vertical" flexItem />
        <Grid item mr={1.3} ml={1.3} sx={{ paddingTop: "0 !important" }}>
          <Link href="#" underline="none">
            All Questions
          </Link>
        </Grid>
        <Divider light orientation="vertical" flexItem />
        <Grid item mr={1.3} ml={1.3} sx={{ paddingTop: "0 !important" }}>
          <Link href="#" underline="none">
            Terms
          </Link>
        </Grid>
        <Divider light orientation="vertical" flexItem />
        <Grid item mr={1.3} ml={1.3} sx={{ paddingTop: "0 !important" }}>
          <Link href="#" underline="none">
            Privacy Policy
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
}
