import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { Grid, Typography, Button, IconButton, paginationClasses } from "@mui/material";
import Chip from "@mui/material/Chip";
import PlayCircle from "@mui/icons-material/PlayCircle";
import DriveEtaIcon from "@mui/icons-material/DriveEta";
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing';
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ResponsiveAppBar from "./AppBar";
import ImgMediaCard from "./ImgMediaCard";
import QuestionsBox from "./QuestionsBox";
import Footer from "./Footer";
import {makeGetCall} from "../services/api/index"





export default function App(props) {
  
  const [jobID, setJobID] = useState(0)
  const [topicID, setTopicID] = useState(0)
  const [topicTitle, settopicTitle] = useState("")
  const [renderedMainJobs, setRenderedMainJobs] = useState([])
  const [renderedListJobs, setRenderedListJobs] = useState([])
  const [jobTopics, setJobTopics] = useState([])
  const [renderedJobTopics, setRenderedJobTopics] = useState([])
  const [renderedTopicQuestions, setRenderedTopicQuestions] = useState([])
  const [jobsLoaded, setJobsLoaded] = useState(false)
  useEffect(() => {
    let jobsForMain = [];
    let jobsForList = [];
    if (!jobsLoaded){
      makeGetCall("job/", function(res){
        setJobsLoaded(true) 
        res.forEach(element => {
          let icon = ""
          if (element.icon == '1')
            icon = <DriveEtaIcon />
          else if (element.icon == '2')
            icon = <PrecisionManufacturingIcon />
          jobsForMain.push(
            <Grid item xs={"auto"} mt={9} align="center">
              <Chip
                icon={icon}
                onClick={() => {
                    setJobID(element.id);
                    setTopicID(0)
                    location.href = "#Topics"
                }}
                color="primary"
                label={element.title}
              />
            </Grid>
          )
        });
        jobsForList.push(<ImgMediaCard for="Jobs" items={res} sID={[setJobID, setTopicID]}/>)
        setRenderedMainJobs(jobsForMain)
        setRenderedListJobs(jobsForList)
        let jobTopics = [];
        makeGetCall('topic/', function(res){
          jobTopics=[<ImgMediaCard for="Topics" items={res} sID={[setJobID, setTopicID]}/>];
          setJobTopics(res)
          setRenderedJobTopics(jobTopics);
        });
      });
    }
  }, [jobsLoaded]);

  useEffect(() => {
    if (jobID != 0) {
      let jobTopics = [];
      const url = 'topic/filter/?jobID=' + jobID;      
      makeGetCall(url, function(res){
        jobTopics=[<ImgMediaCard for="Topics" items={res} sID={[setJobID, setTopicID]}/>];
        setJobTopics(res)
        setRenderedJobTopics(jobTopics);
      });
    }
  }, [jobID]);

  useEffect(() => {
    let topicQuestions = [];

    if (topicID != 0) {
      settopicTitle(jobTopics.find(item => item.id==topicID).title)
      const url = `question/filter/?topicID=${topicID}`;
      makeGetCall(url, function(res){
        setRenderedTopicQuestions(res);
      });
    }
  }, [topicID]);
  
  return (
    <div>
      <div id="HomeMainImage">        
        <ResponsiveAppBar/>
        <Grid container spacing={1}>
          <Grid item xs={3} mt={5} ml={5} align="center">
            <Grid container id="AAA">
              
              <Grid item xs={12} align="center">
                <Typography
                  variant="h2"
                  component="h2"
                  style={{ color: "white", fontWeight: "bold" }}
                >
                  Prepare For Top Robotics Jobs
                </Typography>
              </Grid>
              <Grid item xs={12} mt={2} mb={6} align="center">
                <Button variant="contained">Let's start</Button>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={7}>
            <Grid container spacing={1} id="main-image">
              {renderedMainJobs}
              <Grid item xs={12} mt={5} align="center">
                <IconButton color="primary">
                  <PlayCircle style={{ fontSize: "120px" }} />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
      {renderedListJobs}
      <div style={{textAlign: "center"}}>
        <ArrowDropDownIcon sx={{ fontSize: 100 }} />
      </div>
      {renderedJobTopics}
      <div style={{textAlign: "center"}}>
        <ArrowDropDownIcon sx={{ fontSize: 100 }} />
      </div>
      {renderedTopicQuestions.length > 0 ? <QuestionsBox questions={renderedTopicQuestions} topicTitle={topicTitle}/> : ""}
      
      <Footer />
    </div>
  );
}

const appDiv = document.getElementById("app");
render(<App />, appDiv);
