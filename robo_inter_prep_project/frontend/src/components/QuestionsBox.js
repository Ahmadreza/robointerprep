import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Button,
  IconButton,
  Card,
  Box,
  ButtonGroup,
  ListItemButton,
  ListItem,
  ListItemText,
  CardContent,
  CardHeader,
  CardActions,
} from "@mui/material";
import { FixedSizeList } from "react-window";

export default function QuestionsBox(props) {
  const questions = props.questions;
  const topicTitle = props.topicTitle;
  const [questionIndex, setQuestionIndex] = useState(0)
  const [questionDescription, setQuestionDescription] = useState(questions[0].description)
  const [answerDescription, setAnswerDescription] = useState(questions[0].answer)
  const [showAnswer, setShowAnswer] = useState(false)

  useEffect(() => {    
    setQuestionDescription(questions[questionIndex].description);
    setAnswerDescription(questions[questionIndex].answer)
    setShowAnswer(false)
  }, [questionIndex]);

  function renderRow(props) {
    const { index, style } = props;
    
    return (
      <ListItem style={style} key={index} component="div" disablePadding>
        <ListItemButton>
          <ListItemText primary={questions[index].title} 
            onClick={() => {
              setQuestionIndex(index);
            }} />
        </ListItemButton>
      </ListItem>
    );
  }
  return (
    <Card sx={{ backgroundColor: "#fbffff", margin: "50px", marginTop: "0" }}>
      <CardHeader title={`${topicTitle} Interview Questions`}></CardHeader>
      <CardContent>
        <Grid container>
          <Grid item xs={3}>
            <FixedSizeList
              height={500}
              itemSize={46}
              itemCount={props.questions.length}
              overscanCount={5}
            >
              {renderRow}
            </FixedSizeList>
          </Grid>
          <Grid item xs={8} ml={2}>
            <ButtonGroup disableElevation variant="contained" color="primary">
              <Button size="small"
                onClick={() => {
                  if (questionIndex > 0)
                    setQuestionIndex(questionIndex - 1)
                }}>Back</Button>
              <Button size="small"
                onClick={() => {
                  if (questionIndex < questions.length - 1)
                    setQuestionIndex(questionIndex + 1)
                }}>Next</Button>
            </ButtonGroup>
            <Card m={2}>
              <CardContent>
                <Typography paragraph={true} sx={{ fontWeight: "530" }}>
                  {questionDescription}
                </Typography>
              </CardContent>
              <CardActions>
                <Button variant="contained"
                  onClick={() => setShowAnswer(!showAnswer)}
                  >{showAnswer ? "HIDE ANSWER" : "SHOW ANSWER"} </Button>
              </CardActions>
            </Card>
            {showAnswer ? 
            <>
            <br />
            <Card m={2}>
              <CardContent>
                <Typography paragraph={true} sx={{ fontWeight: "530" }}>
                  {answerDescription}
                </Typography>
              </CardContent>
            </Card>
            </>
            : ""}
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
