import React, { useState, useEffect } from "react";
import {
  Card,
  Typography,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Box,
  IconButton,
  Grid,
} from "@mui/material";

import PlayCircle from "@mui/icons-material/PlayCircle";

export default function ImgMediaCard(props) {
  const typo = props.for === "Jobs" ? "I want to become a ... engineer" : "You need to solve problems in ....";
  const cards = props.items.map((item) => (    
      <Card
        sx={{
          maxWidth: 245,
          maxHeight: 250,
          borderRadius: 10,
          position: "relative",
        }}
        onClick={() => {
          if (props.for === "Jobs") {
            props.sID[0](item.id);
            props.sID[1](0);
          }
          else
            props.sID[1](item.id);
            
          location.href = props.for == "Jobs"? "#Topics" : "#Questions"
      }}
      >
        <CardMedia
          component="img"
          alt="green iguana"
          height="170px"
          image={item.image}
        />

        <Typography
          variant="h6"
          component="h6"
          sx={{
            position: "absolute",
            top: "5%",
            width: "100%",
            textAlign: "center",
            backgroundColor: "rgba(255, 255, 255, 0.54)",
            fontWeight: "bold",
          }}
        >
          {item.title}
        </Typography>
        {item.yt_video != "" ?
        <IconButton
          aria-label="delete"
          sx={{
            position: "absolute",
            top: "15%",
            width: "100%",
            color: "rgb(255 255 255 / 54%)",
          }}
        >
          <PlayCircle
            sx={{
              fontSize: "100px",
            }}
          />
        </IconButton> : ""}
        <CardContent sx={{ marginTop: 0 }}>
          <Grid container>
            
          { props.for === "Jobs" ? 
            <Grid item xs={3}>
              <Typography
                variant="h5"
                component="h5"
                sx={{
                  fontWeight: "bold",
                }}
              >
                {item.topics_count}
              </Typography>
              <Typography
                color="text.secondary"
                sx={{
                  fontSize: "0.9rem",
                  fontWeight: "400",
                }}
              >
                Topics
              </Typography>
            </Grid> : ""}
            <Grid item xs={5}>
              <Typography
                variant="h5"
                component="h5"
                sx={{
                  fontWeight: "bold",
                }}
              >
                {item.questions_count}
              </Typography>
              <Typography
                color="text.secondary"
                sx={{
                  fontSize: "0.9rem",
                  fontWeight: "400",
                }}
              >
                Questions
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography
                variant="h5"
                component="h5"
                sx={{
                  fontWeight: "bold",
                }}
              >
                10%
              </Typography>
              <Typography
                color="text.secondary"
                sx={{
                  fontSize: "0.9rem",
                  fontWeight: "400",
                }}
              >
                Completed
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    ))
  return (
    <div style={{ margin: "40px" }} id={props.for}>
      <Typography variant="h3" component="h3" color="text.primary" mb={3}>
      {typo}
      </Typography>
      <Box sx={{ display: "flex", flexDirection: "row", columnGap: "20px" }}>
        {cards}
      </Box>
    </div>
  );
}
