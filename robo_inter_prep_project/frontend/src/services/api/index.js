const baseAPIsURL = "http://127.0.0.1:8000/api/"

const makePostCall = function (url, data, callback) {
	fetch(baseAPIsURL + url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	}).then(function (response) {
		if (response.ok) {
			return response.json();
		}
		return Promise.reject(response);
	}).then(function (data) {
		// If there's a callback function, run it
		if (callback && typeof callback === 'function') {
			callback(data);
		}
	}).catch(function (error) {
		console.warn(error);
	});
};


const makeGetCall = function (url, callback) {
	fetch(baseAPIsURL + url, {
		method: 'GET',
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	}).then(function (response) {
		if (response.ok) {
			return response.json();
		}
		return Promise.reject(response);
	}).then(function (data) {
		// If there's a callback function, run it
		if (callback && typeof callback === 'function') {
			callback(data);
		}
	}).catch(function (error) {
		console.warn(error);
	});
};

export {
    makeGetCall,
    makePostCall
}